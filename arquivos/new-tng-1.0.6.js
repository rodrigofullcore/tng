// ------------------- Slick in tipbar
$(window).width() <= 1024  ? $('.header__tipbar .services').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1
}) : ''


if ($('body').hasClass('catalog')) {
    // ------------------- Titulo da página
    $('.catalog__titlePage').text(vtxctx.categoryName)

    // ------------------- adiciona main.active
    $('body').on('click', 'a.catalog__filter-toogle', function() {
        $('body main').addClass('filter-active')
    })
    $('body').on('click', '.menu-departamento__close', function() {
        $('body main').removeClass('filter-active')
    })
    
    // ------------------- OrderBy
    $.each($('.fullcore__order-by li'), function() {
        var selectedAttr = $(this).attr('data-value');
        
        if (window.location.href.indexOf(selectedAttr) > -1) {
            $(this).addClass('active')
        }
    })
    
    $('body').on('click', '.fullcore__order-by li', function() {
        let { search, pathname } = window.location
        let selectedAttr = $(this).attr('data-value');
        var newUrl = pathname
        let hasParamsO = ((search.indexOf('?') > -1 || search.indexOf('&') > -1) && search.indexOf('O') > -1)
        
        let attrSelectChange = $(".orderBy:eq(0)").find("select").attr("onchange").split("'")[1];
        let currenturl = window.location.pathname + window.location.search;
        
        $('.fullcore__order-by li').removeClass('selected');
        $('.fullcore__order-by li[data-value="' + selectedAttr + '"]').addClass('selected');
        
        if (attrSelectChange.indexOf("productCluster") > 0 && currenturl.indexOf("productCluster") < 0) {
            newUrl = attrSelectChange + `O=${selectedAttr}`
        } else if (attrSelectChange.indexOf("Novidades") > 0) {
            newUrl = attrSelectChange + `O=${selectedAttr}`
        } else {
            if (search.length && !hasParamsO) {
                newUrl += search + `&O=${selectedAttr}`
            } else if (search.length && hasParamsO) {
                let queries = search.replace('?', '').split('&')
                .map(query => {
                    if (~query.indexOf('O=')) {
                        return query.replace(/\=[a-zA-z]+/g, `=${selectedAttr}`)
                    }
                    return query
                })
                .join('&')
                newUrl += `?${queries}`
            } else {
                newUrl += `?O=${selectedAttr}`
            }
        }
        
        newUrl = newUrl.replace(/pg=(\w+)/, 'pg=1').replace(/line=(\w+)/, 'line=1');
        
        window.location = newUrl;
    })
    
    // ------------------- close filter 
    if ($(window).width() <= 1024) {
        $(document).ajaxStop(function(){
            if ($('.department__sidebar__content').hasClass('js-active')) {
                $('.menu-departamento__close').trigger('click')
            }
        });
    }
}