
const vitrineInterativa = {}
vitrineInterativa.searchProducts = {}
vitrineInterativa.similarProducts = {}
vitrineInterativa.replaceSpecialChars = function (s) {
var b = {
  "\u00e7": "c",
  "\u00e6": "ae",
  "\u0153": "oe",
  "\u00e1": "a",
  "\u00e9": "e",
  "\u00ed": "i",
  "\u00f3": "o",
  "\u00fa": "u",
  "\u00e0": "a",
  "\u00e8": "e",
  "\u00ec": "i",
  "\u00f2": "o",
  "\u00f9": "u",
  "\u00e4": "a",
  "\u00eb": "e",
  "\u00ef": "i",
  "\u00f6": "o",
  "\u00fc": "u",
  "\u00ff": "y",
  "\u00e2": "a",
  "\u00ea": "e",
  "\u00ee": "i",
  "\u00f4": "o",
  "\u00fb": "u",
  "\u00e5": "a",
  "\u00e3": "a",
  "\u00f8": "o",
  "\u00f5": "o",
  "\u00c1": "A",
  "\u00c9": "E",
  "\u00cd": "I",
  "\u00d3": "O",
  "\u00da": "U",
  "\u00ca": "E",
  "\u00d4": "O",
  "\u00dc": "U",
  "\u00c3": "A",
  "\u00d5": "O",
  "\u00c0": "A",
  "\u00c7": "C"
};

return (s || '').toLowerCase().replace(/[\u00e0-\u00fa]/g, function (a) {
  return "undefined" != typeof b[a] ? b[a] : a;
}).replace(/\s|\/|\$|\&|\/|\\|\.|\!|\?/g, '-').replace(/\#/g, '');
}

vitrineInterativa.getSimilars = function (elemento) {
const productId = elemento.data('product-id')
console.log('productId', productId, ' || ', 'elemento', elemento)
if (vitrineInterativa.similarProducts[productId]) {
  vitrineInterativa.renderColorVariation(vitrineInterativa.similarProducts[productId], elemento)
} else {
  try {
    fetch(`https://storetng.vtexcommercestable.com.br/api/catalog_system/pub/products/crossselling/similars/${productId}`)
      .then(res => res.json())
      .then(data => {
        console.log('>>>> data >>>>', data)
        vitrineInterativa.similarProducts[productId] = data
        vitrineInterativa.renderColorVariation(data, elemento)
      })
  } catch (error) {
    console.error(error)
  }
}
}

vitrineInterativa.renderColorVariation = function (similars, elemento) {
  try {
    let container = elemento.find('.interactive__colors')
    console.log('container', container[0], ' || ', 'elemento', elemento[0])
    //container.html('')
    let similarIds = []
    
    const colors = similars.map(similar => {
      const item = {
        // productId: similar.productId,
        // link: similar.link,
        RGB: similar.RGB[0],
        Cor: similar.items[0].Cor[0],
      }
      return item
    });
    const similarFiltered = R.uniq(colors);
    console.log('similarFiltered 2 >>>>>', similarFiltered)

    similarFiltered.forEach((item, index, arr) => {
        console.log('item 3 >>>', item.RGB);
        arr.length >= 2 ? $(container).append(`
        <li class="shelf-product__selector--item ${item.Cor}">
            <a class="shelf-product__selector--button animated fadeIn" href="#" style="
              background: rgb(${item.RGB.replace(/;/g, ',')});
              width: 25px;
              height: 25px;
              min-height: auto;
              display: inline-block;
              margin: 0px 5px;
              pointer-events: none;
              cursor: default;
              border: ${item.RGB.replace(/;/g, ',') == '255,255,255' ? '1px #e0e0e0 solid' : '0'};
            " alt="">
            </a>
        </li>
        `) : $('.shelf-product__selector').slideUp()
        
        // similarId.push(item.productId)
    })
  } catch (error) {
    console.error(error)
  }
}

vitrineInterativa.shelfRenderSelectors = function (el, data) {
let tplSizes = ''
let sizesContainer = el.find('.interactive__sizes')
// array para armazer os tamanhos disponíveis
let sizeArr = []
// verifica e popula "sizeArr" com os tamanhos disponíveis
data[0].items.filter(function (item) {
  if (item.sellers[0].commertialOffer.AvailableQuantity > 0) {
    sizeArr.push(item.Tamanho[0])
    // el.find('.interactive__add-to-cart').attr('data-seller', item.sellers[0].sellerId)
  }
})

// monta o html com os tamanhos
sizeArr.forEach(function (item) {

  tplSizes += `<span class="sr_${vitrineInterativa.replaceSpecialChars(item)}" data-tamanho="${item}">${item}</span>`
})

if (!sizeArr.length) {
  sizesContainer.remove();
}
if (!sizesContainer.find('span')[0]) {
  sizesContainer.append(tplSizes).addClass("is-loaded")
}

if (!el.data('loaded')) {
  el.data('loaded', true)

  el.find('.interactive__sku-variation-size-button').on('click', function (e) {
    e.preventDefault()

    el.find('.interactive__sku-variation-size').toggleClass('is-active')
  })

  sizesContainer.on('click', 'span', function (e) {
    e.preventDefault()

    sizesContainer.find('span').removeClass('active')
    $(this).addClass('active')

    const skuSelectedValue = $(this).data('tamanho')
    // reset
    el.find('.interactive__sku-variation-size-button span').html('')

    el.find('.interactive__sku-variation-size-button').append(`<span>${skuSelectedValue}</span>`)

    el.find('.interactive__sku-variation-size-button').data('selected-size', skuSelectedValue)

    el.find('.interactive__add-to-cart').removeClass('error')
    el.find('.interactive__sku-variation-size').removeClass('is-active')
    el.find(".interactive__sku-variation-size-button").addClass("is--selected")

    const inputEl = el.find('.interactive__quantity-input')
    const inputVal = +inputEl.val()

    data[0].items.filter(item => {
      if (item.Tamanho[0] == skuSelectedValue) {

        let currentSkuQty = item.sellers[0].commertialOffer.AvailableQuantity

        if (inputVal > currentSkuQty)
          inputEl.val(currentSkuQty < 10 ? `0${currentSkuQty}` : currentSkuQty)


        let seller = (item.sellers || []).find(s => s.sellerDefault)
        if (!seller) {
          seller = item.sellers[0]
        }

        el.find('.interactive__add-to-cart').attr('data-seller', seller.sellerId) //
      }
    })
  }).find('span:only-child').trigger('click')

  el.find('.shelf-product__caption').on('click', '.quantity-action', function (e) {
    e.preventDefault()
    const action = $(this).data('action')
    const inputEl = el.find('.interactive__quantity-input')
    const inputVal = +inputEl.val()

    if (action === 'minus') {
      if (inputVal >= 2 && inputVal <= 10) {
        inputEl.val(`0${inputVal - 1}`)
      } else if (inputVal > 10) {
        inputEl.val(inputVal - 1)
      }
    } else if (action === 'plus') {
      if (inputVal < currentSkuQty) {
        inputVal <= 8 ? inputEl.val(`0${inputVal + 1}`) : false
        inputVal >= 9 ? inputEl.val(inputVal + 1) : false
      }
    }
  })

  el.find('.interactive__add-to-cart').on('click', function (e) {
    e.preventDefault()

    if (el.find('.interactive__sku-variation-size-button').data('selected-size') === undefined) {
      el.find('.interactive__sku-variation-size').addClass('is-active')
      el.find('.interactive__add-to-cart').addClass('error')
      return
    }

    const seller = el.find('.interactive__add-to-cart').attr('data-seller') || '1'
    let item = {
      quantity: 1,
      seller: seller
    }

    const skuSelectedValue = el.find('.interactive__sku-variation-size-button').data('selected-size')

    data[0].items.forEach(function (it) {
      if (it.Tamanho[0] == skuSelectedValue) item.id = it.itemId
    })

    vtexjs.checkout.getOrderForm()
      .done(orderForm => saveCartOnBuyingRecurrence(orderForm, item))
  })
}
}
$(document).ready(function () {
$('body').on('hover', '.prateleira .shelf-product:not(.verified)', function (e) {
  $(this).addClass('verified')
  const el = $(e.target).closest('.shelf-product')
  const productId = el.data('product-id')
  console.log('el', el, ' | ', 'productId', productId)
  let currentSkuQty = 99999

  if (el.hasClass('js-requested-selectors')) {
    return false;
  } else {
    el.addClass('js-requested-selectors')
  }

  vitrineInterativa.getSimilars($(this))

  if (vitrineInterativa.searchProducts[productId]) {
    vitrineInterativa.shelfRenderSelectors(el, vitrineInterativa.searchProducts[productId])
  } else {
    fetch(`https://storetng.vtexcommercestable.com.br/api/catalog_system/pub/products/search/?fq=productId:${productId}`, {
        method: 'GET',
        mode: 'cors',
      })
      .then(res => res.json())
      .then(data => {
        vitrineInterativa.searchProducts[productId] = data
        vitrineInterativa.renderColorVariation(data, el);
        vitrineInterativa.shelfRenderSelectors(el, data);
      })
      .catch(error => {
        console.error(error)
      })
  }
})
// limitCharsinShelf();
})

$(document).ajaxStop(function () {
// limitCharsinShelf();
});
