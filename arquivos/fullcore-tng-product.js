const $body = $('body')

class SimulateShipping {
    constructor() {
        let self = this
        let zipCodeEl = '.js-simulate-shipping-postal-code'
        
        this.seller = skuJson.skus[0].sellerId
        this.skuId = skuJson.skus[0].sku
        this.country = 'BRA'
        this.salesChannel = +window.jssalesChannel || 1
        this.containerElement = $('.js-simulate-shipping-result')
        
        // $body.on('input', zipCodeEl, () => { formatInput(zipCodeEl, '00000-000') })
        
        $body.on('submit', '.js-simulate-shipping', function (e) {
            e.preventDefault()
            if (skuJson.skus[0].available) {
                let postalCode = $(zipCodeEl).val().replace('-', '')
                if (postalCode.length === 8) {
                    self.postalCode = postalCode
                    self.requestShippingInfo()
                }
                else {
                    swal.fire({
                        text: 'Insira um CEP válido',
                        icon: 'warning',
                    })
                }
            }
            else {
                swal.fire({
                    text: 'Este produto está indisponível',
                    icon: 'warning',
                })
            }
        })
        
        $body.on('click', '.js-simulate-shipping-close', function (e) {
            e.preventDefault()
            self.containerElement.slideUp(300)
        })
    }
    
    requestShippingInfo() {
        vtexjs.checkout.getOrderForm()
        .then(orderForm => {
            const item = {
                id: this.skuId,
                seller: this.seller,
                quantity: +$('.js-quantity-value').val() || 1,
            }
            return vtexjs.checkout.simulateShipping([item], this.postalCode, this.country, this.salesChannel)
        })
        .done(shippingInfo => {
            let { slas } = shippingInfo.logisticsInfo[0]
            if (slas && slas.length) {
                this.renderShippingResult(slas)
            }
            else {
                swal.fire({
                    text: 'Não há informações de frete relacionadas a este produto',
                    icon: 'warning',
                })
            }
        })
    }
    
    renderShippingResult(slas) {
        let html = `
        <div class="wrapper--sm full-product__shipping--result">
            <button class="full-product__shipping--close js-simulate-shipping-close" type="button">
                <i class="icon icon-close"></i>
            </button>

            <table class="full-product__shipping-table">
                <thead>
                    <tr>
                        <!--<th>Disponibilidade</th>-->
                        <th>Tempo</th>
                        <th>Valor</th>
                    </tr>
                </thead>
            ${slas.map(sla => this.renderShippingTypeRow(sla)).join('')}
            </table>
        </div>
        `
        this.containerElement.html(html).slideDown(300)
    }
    
    renderShippingTypeRow(sla) {
        let { name, price, shippingEstimate } = sla
        let isFree = price === 0 ? true : false
        let days = +shippingEstimate.replace('bd', '')
        let { CurrencyDecimalDigits, CurrencyDecimalSeparator, CurrencyGroupSeparator } = window.currencyFormat
        let daysText = days === 1 ? 'dia útil' : 'dias úteis'
        price = (price / 100).formatMoney(CurrencyDecimalDigits, CurrencyDecimalSeparator, CurrencyGroupSeparator)
        return `
        <tr class="full-product__shipping-table--row">
            <!--<td class="full-product__shipping-table--cell">${name.replace(/(\d\.)/g, '')}</td>-->
            <td class="full-product__shipping-table--cell">
                até <span class="full-product__shipping-table--days">${days} ${daysText}</span>
            </td>
            <td class="full-product__shipping-table--cell">
                <span class="full-product__shipping-table--value">${isFree ? 'Grátis' : `${window.currencySymbol} ${price}`}</span>
            </td>
        </tr>
        `
    }
}
class newProduct {
    constructor() {
        this.getProductData()
        this.imageFirst()
        this.customPrinces()
        this.replaceImage()
        this.changeImage()
        this.nameProduct()
        this.skuSelected = null
        this.skuJson = skuJson
        this.renderSizeSelector()
        this.animations()
        this.bindEvents()
        this.mobile()
        this.slickShelf()
        this.descountStamp()
        this.bindShare()
        this.simulateShipping = new SimulateShipping()
        // $(window).resize(function() {
        // })
        
        $body.on('click', '.full-product__buy', (e) => {
            try {
                const $this = $(e.currentTarget)
                const skuSelected = self.isZoomOpen ? self.zoomSkuSelected : self.skuSelected
                console.log(skuSelected)
                const quantity = 1//$this.closest('.js-buying-area').find('.js-product-quantity-value').val()
                
                let currentText = $this.text();
                
                if (skuSelected) {
                    $this
                    .prop('disabled', true)
                    .find('.js-product-buy-button-text')
                    .text('Adicionando...')
                    
                    const item = {
                        id: skuSelected.sku,
                        quantity: +quantity,
                        seller: skuSelected.sellerId
                    }
                    // vtexjs.checkout.getOrderForm()
                    //     .then(() => {
                    //         $this
                    //         .prop('disabled', false)
                    //         .text(currentText)
                    //     })
                    // .fail(() => window.location.assign(`/checkout/cart/add?sku=${sku}&qty=${quantity}&seller=${sellerId}&sc=${getCookie('VTEXSC') ? (getCookie('VTEXSC')).split('=')[1] : 1}`))
                    
                    vtexjs.checkout.getOrderForm()
                    .then(() => vtexjs.checkout.addToCart([item], null, 1))
                    .then(() => {
                        $this
                        .prop('disabled', false)
                        .text(currentText)
                        // if(window.innerWidth >= 1023) {
                        //     rex.modules.Minicart.open()
                        // } else {
                        //     window.location = 'https://www.tng.com.br/checkout/?goToShipping=true#/cart'
                        // }
                        window.location = 'https://www.tng.com.br/checkout/?goToShipping=true#/cart'
                    })
                    .fail(() => window.location.assign(`/checkout/cart/add?sku=${sku}&qty=${quantity}&seller=${sellerId}&sc=${getCookie('VTEXSC') ? (getCookie('VTEXSC')).split('=')[1] : 1}`))
                    $('html,body').stop(true, true).animate({scrollTop: 0}, 300);
                }
                else {
                    if (!self.isZoomOpen && !$('.product-info__select-size').length) {
                        $('.full-product__size--button').addClass('unselected')
                        $(`<span class="product-info__select-size">Selecione um tamanho</span>`).insertAfter($this)
                        $('.full-product__size').addClass('animated pulse')
                    }
                }
            }
            catch (error) {
                console.error(error)
            }
        })
        
        $body.on('click', '.full-product__size--options button', (e) => {
            e.preventDefault()
            const $this = $(e.currentTarget)
            try {
                const id = +$this.data('sku-id')
                const parent = $this.parent()
                const skuSelected = self.skuJson.skus.find(sku => sku.sku === id)
                const value = $this.find('.full-product__size--size').text()
                
                if (self.isZoomOpen) self.zoomSkuSelected = skuSelected
                else self.skuSelected = skuSelected
                parent.siblings().removeClass('is-selected')
                parent.addClass('is-selected')
                $('.product-info__select-size').remove()
                // $('.full-product__size--options').addClass('bounceOut')
                $('.full-product__size--options, .full-product__size--overlay').removeClass('is-open')
                $('.full-product__size--value').html(`${value}`).addClass('animated fadeInRight')
                $('.full-product__size--text').html(`Alterar`).addClass('is-active')
                $('.full-product__size--button').removeClass('unselected')
                $('.full-product__size--button i').addClass('is-active').html(`
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 490.337 490.337" style="width: 15px;">
                <path d="M229.9 145.379l-47.5 47.5c-17.5 17.5-35.1 35-52.5 52.7-4.1 4.2-7.2 9.8-8.4 15.3-6.3 28.9-12.4 57.8-18.5 86.7l-3.4 16c-1.6 7.8.5 15.6 5.8 20.9 4.1 4.1 9.8 6.4 15.8 6.4 1.7 0 3.4-.2 5.1-.5l17.6-3.7c28-5.9 56.1-11.9 84.1-17.7 6.5-1.4 12-4.3 16.7-9 78.6-78.7 157.2-157.3 235.8-235.8 5.8-5.8 9-12.7 9.8-21.2.1-1.4 0-2.8-.3-4.1-.5-2-.9-4.1-1.4-6.1-1.1-5.1-2.3-10.9-4.7-16.5-14.7-33.6-39.1-57.6-72.5-71.1-6.7-2.7-13.8-3.6-20-4.4l-1.7-.2c-9-1.1-17.2 1.9-24.3 9.1-45 45.2-90.3 90.5-135.5 135.7zm156.5-120.7h.5l1.7.2c5.2.6 10 1.2 13.8 2.8 27.2 11 47.2 30.6 59.3 58.2 1.4 3.2 2.3 7.3 3.2 11.6.3 1.6.7 3.2 1 4.8-.4 1.8-1.1 3-2.5 4.3-78.7 78.5-157.3 157.2-235.9 235.8-1.3 1.3-2.5 1.9-4.3 2.3-28.1 5.9-56.1 11.8-84.2 17.7l-14.8 3.1 2.8-13.1c6.1-28.8 12.2-57.7 18.4-86.5.2-.9 1-2.3 1.9-3.3 17.4-17.6 34.8-35.1 52.3-52.5l47.5-47.5c45.3-45.3 90.6-90.6 135.8-136 1.9-1.6 2.8-1.9 3.5-1.9z"/>
                <path d="M38.9 109.379h174.6c6.8 0 12.3-5.5 12.3-12.3s-5.5-12.3-12.3-12.3H38.9c-21.5 0-38.9 17.5-38.9 38.9v327.4c0 21.5 17.5 38.9 38.9 38.9h327.3c21.5 0 38.9-17.5 38.9-38.9v-167.5c0-6.8-5.5-12.3-12.3-12.3s-12.3 5.5-12.3 12.3v167.5c0 7.9-6.5 14.4-14.4 14.4H38.9c-7.9 0-14.4-6.5-14.4-14.4v-327.3c0-7.9 6.5-14.4 14.4-14.4z"/>
                </svg>
                `)
                setTimeout(() => {
                    $('.full-product__size--value').removeClass('animated fadeInRight')
                }, 1500);
            }
            catch (error) {
                console.error(error)
            }
        })
    }
    share(media) {
        let { skuJson } = this
        let { href } = window.location
        let windowOpts = 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600'
        let url
        switch (media) {
            case 'pinterest':
            url = `http://pinterest.com/pin/create/button/?url=${href}&description=${skuJson.name}&media=${skuJson.skus[0].image}`
            break
            case 'facebook':
            url = `https://www.facebook.com/sharer/sharer.php?u=${href}`
            break
            case 'twitter':
            url = `https://twitter.com/intent/tweet?url=${href}&text=${skuJson.name}`
            break
            case 'whatsapp':
            url = `https://api.whatsapp.com/send?text=${href}`
            break
            default:
            break
        }
        window.open(url, '', windowOpts)
    }
    
    bindShare(){
        let _self = this
        $('.js-share-button').on('click', function (e) {
            _self.share($(this).data('media'))
        })
    }
    
    animations() {
        //$("header").addClass('animated fadeInDown')
        
        // var x=x+1;
        // const element =  document.querySelector('.my-element')
        // element.classList.add('animated', 'bounceOutLeft')
        
        // element.addEventListener('animationend', function() { doSomething() })
    }
    
    descountStamp() {
        var skus = skuJson.skus
        var container = $('.full-product__price')
        
        $.each(skus, function(){
            if (this.available && this.listPrice > this.bestPrice) {
                var desconto = ((this.listPrice - this.bestPrice) / this.listPrice * 100).toFixed(0)
                container.append(`<span class="full-product__discount-flag hide--sm">${desconto} %OFF</span>`)
                $('.full-product__area--images').prepend(`<span class="full-product__discount-flag hide--lg hide--md">${desconto} %OFF</span>`)
                return false
            }
        })
    }
    
    nameProduct() {
        $(".full-product__name").html(skuJson.name)
    }
    
    customPrinces() {
        var listPrice = $('.price-list-price')[0]
        $('.price-best-price').prepend(listPrice)
    }
    
    imageFirst() {
        var src = skuJson.skus[0].image
        $('.full-product__images #include').addClass('easyzoom easyzoom--overlay')
        $('.full-product__images #include').html(`
        <a href="${src.replace('-292-292', '-1200-1643')}">
        <img src="${src.replace('-292-292', '-639-876')}" class="wow fadeIn animated daley-3s"/>
        </a>`)
    }
    
    changeImage() {
        $("body").on('click', '.thumbs a', function(e) {
            e.preventDefault()
            var rel = $(this).attr("rel")
            if ($(this).attr('rel') != $('#include img').attr('src')) {
                $('.full-product__images #include').html(`
                <a href="${rel.replace('-639-876', '-1200-1643')}">
                <img src="${rel}" class="fadeIn animated"/>
                </a>`)
            }
        })
    }
    
    replaceImage() {
        const $thumbs = $(".thumbs")
        $thumbs.find('li').each(function(i, el) {
            const _this = $(this)
            // $(this).addClass('easyzoom easyzoom--overlay')
            $(this).find('img').attr('src')
            $(this).find('a').attr('href', $(this).find('img').attr('src').replace('-110-150', '-1200-1643'))
            if(window.innerWidth >= 1023) {
                var imageThumb = $(this).find('a img').attr('src');
                var relThumb = $(this).find('a').attr("rel").replace('-292-292', '-639-876');
            } else {
                var imageThumb = $(this).find('a img').attr('src').replace('-110-150', '-800-1091');
                var relThumb = $(this).find('a').attr("rel").replace('-292-292', '-800-1091');
            }
            
            $(this).find('a img').attr('src', imageThumb)
            $(this).find('a').attr("rel", relThumb)
        })
        if ($thumbs.find('li').length > 1) {
            $thumbs.slick({
                dots: false,
                infinite: false,
                speed: 300,
                slidesToShow: 4,
                slidesToScroll: 1,
                arrows: true,
                vertical: true,
                autoplay: false,
                lazyLoad: "progressive",
                responsive: [
                    {
                        breakpoint: 1024,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            vertical: false,
                            dots: true,
                            arrows: false
                        }
                    },
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1,
                            vertical: false,
                            dots: true,
                            arrows: false
                        }
                    }
                ]
            });
        }
    }
    
    slickShelf() {
        setTimeout(() => {
            $('.full-product__shelf').each(function() {
                let t = $(this),
                sts = parseInt(t.attr('data-slides-to-show') || 5) ,
                stsNew = t.find('.prateleira > ul > li ').length;
                sts = Math.max(1, Math.min(stsNew,  sts));
                t.find('.prateleira > ul').slick({
                    infinite: false,
                    slidesToShow: 5,
                    slidesToScroll: 1,
                    dots: false,
                    autoplay: false,
                    prevArrow: '<div class="slick-arrow slick-prev"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23px" height="48px"> <path fill-rule="evenodd"  fill="rgb(90, 90, 90)" d="M20.999,47.025 L-0.001,24.203 L20.999,-0.001 L22.999,2.764 L3.823,24.100 L22.999,45.000 L20.999,47.025 Z"/></svg></div>',
                    nextArrow: '<div class="slick-arrow slick-next"><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="23px" height="47px"><path fill-rule="evenodd"  fill="rgb(90, 90, 90)" d="M2.000,0.006 L23.000,22.812 L2.000,47.000 L-0.001,44.237 L19.176,22.916 L-0.001,2.029 L2.000,0.006 Z"/></svg></div>',
                    lazyLoad: "progressive",
                    responsive: [
                        {
                            breakpoint: 500,
                            settings: {
                                arrows: true,
                                dots: false,
                                swipeToSlide: false,
                                slidesToShow: 1,
                                slidesToScroll: 1,
                            }
                        },
                        {
                            breakpoint: 1024,
                            settings: {
                                arrows: true,
                                dots: false,
                                swipeToSlide: false,
                                slidesToShow: 3,
                                slidesToScroll: 3,
                            }
                        },
                    ]
                });
            })
        }, 1000);
    }
    
    zoom() {
        $(window).on("ProductFinished", () => {
            let fix_zoom = function() {
                window.LoadZoom = function(pi) {
                    let zoom = $(".product__canvas-inner")
                    .addClass("easyzoom easyzoom--overlay")
                    .easyZoom();
                    
                    window.zoomAPI = zoom.data("easyZoom");
                    window.ImageControl = () => null;
                };
                
                LoadZoom(0);
            };
            $(fix_zoom);
            
            $(".product__gallery").on("click", ".thumbs__link", function() {
                const image = $(".thumbs__image", this).attr("src");
                $(".product__canvas-inner a").attr("href", image);
                $(".product__canvas-inner img").attr("src", image);
                zoomAPI._init();
                zoomAPI.swap(image, image);
                
                $(fix_zoom);
            });
        });
    }
    
    replaceSpecialChars(s) {
        var b = {
            "\u00e7": "c","\u00e6": "ae","\u0153": "oe","\u00e1": "a","\u00e9": "e","\u00ed": "i","\u00f3": "o","\u00fa": "u","\u00e0": "a","\u00e8": "e","\u00ec": "i","\u00f2": "o","\u00f9": "u","\u00e4": "a","\u00eb": "e","\u00ef": "i","\u00f6": "o","\u00fc": "u","\u00ff": "y","\u00e2": "a","\u00ea": "e","\u00ee": "i","\u00f4": "o","\u00fb": "u","\u00e5": "a","\u00e3": "a","\u00f8": "o","\u00f5": "o","\u00c1": "A","\u00c9": "E","\u00cd": "I","\u00d3": "O","\u00da": "U","\u00ca": "E","\u00d4": "O","\u00dc": "U","\u00c3": "A","\u00d5": "O","\u00c0": "A","\u00c7": "C"
        }
        
        return (s || '').toLowerCase().replace(/[\u00e0-\u00fa]/g, function(a) {
            return "undefined" !== typeof b[a] ? b[a] : a
        }).replace(/\s|\/|\$|\&|\/|\\|\.|\!|\?/g, '-').replace(/\#/g, '')
    }
    
    getProductData() {
        try {
            const { productId } = skuJson
            fetch(`/api/catalog_system/pub/products/search/?fq=productId:${productId}`, {
                method: 'GET',
            })
            .then(res => {
                if (res.ok) return res.json()
                throw Error()
            })
            .then(data => {
                this.data = data[0]
                this.getSimilars()
            })
            .catch(console.error)
        }
        catch(error) {
            console.error(error)
        }
    }
    
    getSimilars() {
        try {
            const { productId } = skuJson
            const { productName } = this.data
            
            fetch(`/api/catalog_system/pub/products/crossselling/similars/${productId}`, {
                method: 'GET'
            })
            .then(res => res.json())
            .then(data => {
                this.similars = data
                this.renderColorVariation()
                this.renderSizeSelector()
            })
        }
        catch (e) {
            console.error(e)
        }
    }
    
    renderColorVariation(container = '.js-product-color') {
        try {
            // $('.full-product__selector').removeClass('hide--all')
            let similarId = []
            console.log('>>>> data >>>', this)
            
            $(container).parent().addClass('grid').prepend(`
            <label>
                cor: <span>${this.data.items[0].Cor[0]}</span>
            </label>
            `)

            $(container).addClass('grid grid--left').append(`
            <li class="full-product__selector--item is-selected">
                <a class="full-product__selector--button" style="background: rgb(${this.data.RGB[0].replace(/;/g, ',')});" alt="${this.data.productName}">
                    <!--<img class="full-product__selector--image" src="${this.data.items[0].images[0].imageUrl}" alt="${this.data.productName}" />
                </a>
            </li>
            `)


            const colors = this.similars.map(similar => {
                const item = {
                    productId: similar.productId,
                    link: similar.link,
                    RGB: similar.RGB[0],
                    Cor: similar.items[0].Cor[0],
                }
                return item
            });
            
            
            const similarFiltered = R.uniq(colors);
            console.log('similarFiltered >>>>> ', similarFiltered)
            
            similarFiltered.forEach((item, index, arr) => {
                console.log('item 3 >>>', arr.length >= 2);
                arr.length >= 2 ? $(container).append(`
                <li class="full-product__selector--item ${item.Cor}">
                    <a class="full-product__selector--button animated fadeIn" href="${item.link}" style="background: rgb(${item.RGB.replace(/;/g, ',')});border: ${item.RGB.replace(/;/g, ',') == '255,255,255' ? '1px #e0e0e0 solid' : '0'};" alt="${this.data.productName}">
                    </a>
                </li>
                `) && $('.full-product__selector').css('visibility', 'initial') : $('.full-product__selector').slideUp()
                
                similarId.push(item.productId)
            })
            similarId.length == 0 ? $('.full-product__selector').slideUp() : ''
            
        }
        catch (e) {
            console.error(e)
        }
    }
    
    renderSizeSelector(container = '.full-product__size--options') {
        let _this = this
        let { skus, dimensionsMap } = this.skuJson
        let { Tamanho: sizes } = dimensionsMap
        let availableSkus = skus.filter(s => s.available == true)
        let singleSku = false
        let isSelected = false
        
        if (availableSkus.length > 1) availableSkus = false
        
        const { skuSelected } = this;
        
        let options = sizes.slice(0).map(size => {
            let sku = skus.find(sku => sku.dimensions.Tamanho === size)
            let {
                available,
                availablequantity,
                sellerId,
                sku: skuId,
            } = sku
            
            if (skuSelected) {
                isSelected = skuSelected.sku === skuId ? true : false
            }
            
            if (availableSkus) {
                if (availableSkus[0].sku === skuId) {
                    singleSku = true
                    _this.skuSelected = availableSkus[0]
                    _this.zoomSkuSelected = availableSkus[0]
                }
                else {
                    singleSku = false
                }
            }
            return `
            <li class="full-product__size--item ${available ? '' : 'is-unavailable'} ${isSelected ? 'is-selected' : ''} ${singleSku ? 'is-selected' : ''}">
            <button class="sr_${_this.replaceSpecialChars(size)} full-product__size--option" data-sku-id="${skuId}" data-seller-id="${sellerId}" ${available ? '' : 'disabled'}>
            <span class="full-product__size--size">${size}</span>
            ${availablequantity > 0 && availablequantity <= 2 ? `
            <span class="full-product__size--tooltip">só tem ${availablequantity}</span>
            ` : ''}
            </button>
            </li>
            `
        })
        $(container).html(options.join(''))
        $('.full-product__size--options li').length <= 1 ? $('.full-product__size--option').trigger('click') && $('.full-product__size--text').html(`Tamanho único`) : ''
    }
    
    mobile() {
    }
    
    bindEvents() {
        const _this = this
        $body.on('click', '.item-dimension-Tamanho label', function() {
            $(document).ajaxStop(function(){
                _this.replaceImage()
            });
        })
        
        $body.on('click', '.full-product__size--button', function(e) {
            e.stopPropagation()
            const $this = $(e.currentTarget)
            const $target = $('.full-product__size--options')
            
            $target.toggleClass('is-open')
            $('.full-product__size--overlay').toggleClass('is-open')
        })
        $body.on('click', '.full-product__size--overlay', function(e) {
            const $this = $(e.currentTarget)
            const $target = $('.full-product__size--options')
            $('.full-product__size--overlay').toggleClass('is-open')
            $target.toggleClass('is-open')
        })
        
        $body.on('click', '.full-product__selector--button', function(e) {
            e.preventDefault()
            const $this = $(e.currentTarget)
            const url = $this.attr('href')
            
            // $body.removeClass('remove')
            $body.addClass('no-scroll')
            $('.full-product__overlay').addClass('is-active')
            
            setTimeout(function(){
                location.replace(`${url}`)
            }, 1000);
        })
        
        $body.on('click', '.full-product__actions--button', function(e) {
            const $this = $(e.currentTarget)
            const data = $this.data('content')
            
            if ($this.find('span').text() != 'Tabela de Medidas') {
                $('.full-product__actions button').removeClass('active')
                $this.addClass('active')
                
                $('.full-product__action--content .full-product__action--box').removeClass('active fadeIn')
                $('.full-product__action--content [data-content="'+data+'"]').addClass('active animated fadeIn')
            } else {

            }
        })
        $body.on('click', '.full-product__action--close', function(e) {
            $('.full-product__actions button').removeClass('active')
            $('.full-product__action--content .full-product__action--box').removeClass('active fadeIn')
        })
    }
}

if ($('body').hasClass('new-product')) {
    new SimulateShipping()
    new newProduct()
}

$(window).load(function() {
    // Preloader
    $('.full-product__overlay').delay(2000).removeClass('is-active')
    if(window.innerWidth <= 1023) {
        $('.bread-crumb').removeClass('wow')
        $('#show').removeClass('animated fadeIn delay-1s');

        $('.full-product__area-floater .full-product__panel, .full-product__purchase, .full-product__experimente').attr('data-wow-duration', '.5s')
        $('.full-product__area-floater .full-product__panel, .full-product__purchase, .full-product__experimente').attr('data-wow-delay', '0')
        
        $('.bread-crumb').delay(3000).addClass('animated fadeInUp delay-1s')
        $('#show').delay(3000).addClass('animated fadeIn delay-1s')
        $('.full-product__discount-flag').addClass('animated rubberBand delay-5s"')
        $('.full-product__social').addClass('animated rubberBand delay-5s"')
    }
    
    // Animação
    wow = new WOW ({
        //mobile: false
    })
    wow.init();
    
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom({
        loadingNotice: 'Carregando',
        errorNotice: 'Ocorreu um erro',
        preventClicks: false
    });

    // Get an instance API
    var zoomAPI = $easyzoom.data('easyZoom');

});